createClient = require 'ipfs-http-client'

{ IPFS_API = 'http://localhost:5001' } = process.env

module.exports = ( options ) ->

  seneca = @
  node = null

  @add 'role:pubsub,action:pub', ( msg, respond ) ->
    respond null
    message = msg[ msg.in ]

    node.pubsub.publish 'tg', message
      .then console.log
      .catch console.error

  @add 'role:pubsub,action:sub', ( msg, respond ) ->
    respond null
    { topic = 'lorem', pattern } = msg

    node.pubsub.subscribe topic, ( message ) ->
      return if not pattern

      seneca.act pattern, message, process.stdout

  @add 'init:pubsub', ( msg, respond ) ->
    node = createClient IPFS_API
    respond null
